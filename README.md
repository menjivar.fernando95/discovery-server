# Discovery Server
Aplicacion para manejar los microservicios



## Features

- manejar los microservicios
- asignar puertos y proporcionar los puertos mediante name-id
- manejo de replicas 

And of course Dillinger itself is open source with a [public repository][dill]
 on GitHub.

## Installation
Importar el proyecto con intellijidea y aplicar correr el comando mvn install
Install the dependencies and devDependencies and start the server.

```sh
mvn install
```

Start...
Se puede correr desde el iDE o ejecutar el siguiente comando abriendo una consola y estar en el directorio target donde se guarda el compilado

```sh
java -jar discovery-server-0.0.1-SNAPSHOT.jar
```
### Se debe tener instalado java 11, y correra en el puerto local :  http://localhost:8761/

## License

MIT

**Free Software, Hell Yeah!**

